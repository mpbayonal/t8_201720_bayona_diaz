package test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.DirectedGraph;
import model.data_structures.Paths;

public class PathsTest extends TestCase {

	DirectedGraph<Integer, Integer> grafo;
	protected void setUp() throws Exception {
		grafo = new DirectedGraph<Integer, Integer>();
	}

	public void testCamino() throws Exception {
		grafo.addVertex(1, 1);
		grafo.addVertex(2, 2);
		grafo.addVertex(3, 3);
		grafo.addVertex(4, 4);
		grafo.addVertex(5, 5);
		grafo.addVertex(6, 6);
		grafo.addEdge(1, 2, 1.4);
		grafo.addEdge(1, 5, 1.4);
		grafo.addEdge(2, 3, 1.4);
		grafo.addEdge(3, 4, 1.4);
		grafo.addEdge(3, 5, 1.4);
		grafo.addEdge(4, 5, 1.4);
		grafo.addEdge(5, 2, 1.4);
		
		Paths<Integer, Integer> camino = new Paths<Integer, Integer>(1, grafo);
		Iterator<Integer> ite = camino.camino(5).iterator();
		
		assertEquals("WCEX",ite.next().intValue(), 1 );
		assertEquals("WCEX",ite.next().intValue(), 2 );
		assertEquals("WCEX",ite.next().intValue(), 3 );
		assertEquals("WCEX",ite.next().intValue(), 4 );
		assertEquals("WCEX",ite.next().intValue(), 5 );
		
		
	}

	public void testExisteCamino() throws Exception {
		grafo.addVertex(1, 1);
		grafo.addVertex(2, 2);
		grafo.addVertex(3, 3);
		grafo.addVertex(4, 4);
		grafo.addVertex(5, 5);
		grafo.addVertex(6, 6);
		grafo.addVertex(7, 7);
		grafo.addEdge(1, 2, 1.4);
		grafo.addEdge(1, 5, 1.4);
		grafo.addEdge(2, 3, 1.4);
		grafo.addEdge(3, 4, 1.4);
		grafo.addEdge(3, 5, 1.4);
		grafo.addEdge(4, 5, 1.4);
		grafo.addEdge(5, 2, 1.4);
		
		
		
		Paths<Integer, Integer> camino = new Paths<Integer, Integer>(1, grafo);
		boolean hay = camino.existeCamino(5);
		boolean hay2 = camino.existeCamino(7);
		
		assertEquals("WCEX",hay, true );
		assertEquals("WCEX",hay2, false );
	}

}
