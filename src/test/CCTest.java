package test;

import junit.framework.TestCase;
import model.data_structures.CC;
import model.data_structures.DirectedGraph;

public class CCTest extends TestCase {

	DirectedGraph<Integer, Integer> grafo;
	protected void setUp() throws Exception {
		grafo = new DirectedGraph<Integer, Integer>();
		
	}

	public void testStronglyConnected() throws Exception {
		grafo.addVertex(1, 1);
		grafo.addVertex(2, 2);
		grafo.addVertex(3, 3);
		grafo.addVertex(4, 4);
		grafo.addVertex(5, 5);
		grafo.addVertex(6, 6);
		grafo.addEdge(1, 3, 1.4);
		grafo.addEdge(1, 2, 1.4);
		grafo.addEdge(2, 5, 1.4);
		grafo.addEdge(3, 4, 1.4);
		grafo.addEdge(3, 5, 1.4);
		grafo.addEdge(4, 5, 1.4);
		grafo.addEdge(5, 1, 1.4);
		
		CC<Integer, Integer> cc = new CC<Integer, Integer>(grafo);
		boolean conectados = cc.stronglyConnected(1, 2);
		boolean conectados2 = cc.stronglyConnected(6, 2);
		boolean conectados3 = cc.stronglyConnected(3, 1);
		
		assertEquals("WCEX",conectados, true );
		assertEquals("WCEX",conectados2, false );
		assertEquals("WCEX",conectados3, true );
		
	}

}
