package model.logic;



public class VOHoraParadaPorParada implements Comparable<VOHoraParadaPorParada>{
	
	  public VOHoraParadaPorParada(int tripId, String arrivalTime, String departureTime) {
		super();
		this.tripId = tripId;
		this.arrivalTime = arrivalTime;
		this.departureTime = departureTime;
		
	}
	  
	  public int hashCode() {
	    	return tripId;
	    }

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	private int tripId;
	    private String arrivalTime;
	    private String departureTime;
	    private int idRoute;
	  
		@Override
		public int compareTo(VOHoraParadaPorParada o) {
			// TODO Auto-generated method stub
			return 0;
		}

		public int getIdRoute() {
			return idRoute;
		}

		public void setIdRoute(int idRoute) {
			this.idRoute = idRoute;
		}

}