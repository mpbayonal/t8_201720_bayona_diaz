package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import model.data_structures.CC;
import model.data_structures.Cycle;
import model.data_structures.DirectedGraph;

import model.data_structures.LPHashTable;
import model.data_structures.Paths;
import model.data_structures.QueueIterable;
import model.data_structures.SeparateChainingHashTable;





public class STSManager {
	
	private PersistenceManager persistencia;
	private LPHashTable<Integer, VOTrip> trips;
	public DirectedGraph<Integer, VOStop> stops;

	public void ITSInit() 
	{
		

		persistencia = new PersistenceManager();
		trips = persistencia.loadTrips("./data/trips.txt");
		stops = persistencia.loadStops("./data/stops.txt");
		crearGrafo("./data/stop_times.txt");
		
		System.out.println("se cargaron los datos");


	}
	
	// 2. Implemente un método que indica cuantos componentes conectados tiene el grafo construido.
	
	public int componentesConectadosParadas() throws Exception 
	{
		CC<Integer, VOStop> cc = new CC<Integer, VOStop>(stops);
		
		return cc.count();
	}
	
	//3. Implemente un método que indica si el grafo tiene al menos un ciclo y diga las paradas que se encuentran en un ciclo.
	
	public QueueIterable<VOStop> hayCiclo() throws Exception 
	{	Cycle<Integer, VOStop> cycle = new Cycle<Integer, VOStop>(stops);
	
		QueueIterable<VOStop> ciclo = new QueueIterable<VOStop>();
		Iterator<Integer> ite = cycle.darCiclo().iterator();
		while(ite.hasNext()) 
		{
			Integer actual = ite.next();
			ciclo.enqueue(stops.darVertex(actual));
			
		}
		return ciclo;
	}
	
	// 4. Implemente un método que, dada una parada P1, diga a cuáles paradas se puede llegar a partir
	// de P1. Para cada parada en la respuesta se debe dar su distancia desde P1 de acuerdo al recorrido BFS.
	
	public QueueIterable<ParadaDistancia> darCamino(int parada) throws Exception 
	{	
		
		Paths<Integer, VOStop> caminos = new Paths<Integer, VOStop>(parada, stops);
		
		
	
		QueueIterable<ParadaDistancia> camino = new QueueIterable<ParadaDistancia>();
		Iterator<Integer> ite = stops.darVertices().iterator();
		while(ite.hasNext()) 
		{
			Integer actual = ite.next();
			if(actual != parada && caminos.existeCamino(actual)) 
			{
				Iterable<Integer> caminoActual = caminos.camino(actual);
				Double dist = darDistanciaCaminoParadas(caminoActual);
				ParadaDistancia actual2 = new ParadaDistancia(dist, stops.darVertex(actual));
				
				camino.enqueue(actual2);
				
				
			}
			
			
		}
		return camino;
	}
	

	public Double darDistanciaCaminoParadas(Iterable<Integer> ite) throws Exception 
	{	Double distancia = 0.0;
		Iterator<Integer> it = ite.iterator();
		Integer anterior = it.next();
		
		while(it.hasNext()) 
		{
			Integer actual = it.next();
			
			distancia += stops.darEdge(anterior, actual).getPeso();
			anterior = actual;
		}
		
		return distancia;
		
	}
	public Date parsingDate(String date) throws ParseException 
	{	
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss a");
		DateFormat formatMilitar = new SimpleDateFormat( "HH:mm:ss");
		Date time = null;

		try 
		{
			time = format.parse(date);
		} catch (ParseException e) {

			time = formatMilitar.parse(date);	 
		}

		if (time != null) 
		{
			String formattedDate = formatMilitar.format(time);
		}

		return time;

	}
	
	
	public void crearGrafo(String stopTimesFile) {


		String cadena;
		VOStopTime anterior = null;
		

		FileReader file = null;
		BufferedReader reader = null;
		String datos[];

		try
		{
			file= new FileReader(stopTimesFile);
			reader = new BufferedReader(file);

			boolean firstHeaderRow = true;

			while((cadena = reader.readLine())!=null)
			{
				if(!firstHeaderRow)
				{
					datos = cadena.split(",");

					int tripId = Integer.parseInt(datos[0]);
					String arrivalTime = persistencia.getStringFromCsvLine(datos, 1);
					String departureTime = persistencia.getStringFromCsvLine(datos, 2);
					int stopId = persistencia.getIntFromCsvLine(datos, 3);

					
					int stopSequence = persistencia.getIntFromCsvLine(datos, 4);
					
					Double shapeDistTraveled = 0.0;
					if(datos.length > 8)
					{
						shapeDistTraveled = persistencia.getStringFromCsvLine(datos, 8).isEmpty() ? null : Double.parseDouble(persistencia.getStringFromCsvLine(datos, 8));
					}
					
					VOStopTime newStopTime = new VOStopTime(tripId, arrivalTime, departureTime, stopId, stopSequence, 0, 0, 0, shapeDistTraveled);

					if(newStopTime.getStopSequence()==1) 
					{
						anterior = newStopTime;
					}
					else 
					{
						stops.addEdge(anterior.getStopId(), newStopTime.getStopId(), newStopTime.getShapeDistTraveled());
						anterior = newStopTime;
					}

				}
				else
				{
					firstHeaderRow = false;
				}
			}
		}
		catch (Exception e)
		{
			throw new RuntimeException("No se pudo cargar el archivo. " + e.getMessage(), e);
		}
		finally
		{
			try
			{
				if(file != null)
				{
					file.close();
				}

				if(reader != null)
				{
					reader.close();
				}
			}
			catch(Exception e2)
			{
				throw new RuntimeException("Error al cerrar el archivo. " + e2.getMessage(), e2);
			}
		}

	}

}
