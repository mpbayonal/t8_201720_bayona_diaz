package model.logic;




public class VOTrip implements Comparable<VOTrip>{
	
	  // -----------------------------------------------------------------
    // Attributes
    // -----------------------------------------------------------------

    private int routeId;
    private int serviceId;
    private int tripId;
   
    private String tripShortName;
   
    
   

    // -----------------------------------------------------------------
    // Constructor
    // -----------------------------------------------------------------

    /**
     * @return id - Route's id number
     */
    public VOTrip( int pRouteId, int pServiceId, int pTripId,  String pTripShortName)
    {
        routeId = pRouteId;
        serviceId = pServiceId;
        tripId = pTripId;
       
        tripShortName = pTripShortName;
       
       
    }

    // -----------------------------------------------------------------
    // Methods - Getters
    // -----------------------------------------------------------------
    
   
    
    public int hashCode()
    {
    		return tripId;
    }

    // -----------------------------------------------------------------
    // Methods - Logic
    // -----------------------------------------------------------------

  

	public int compareTo(VOTrip n) {
        int r;

        if(n.getTripId() == tripId) {
            r=0;
        }
        else if(n.getTripId() < tripId) {
            r=1;
        }
        else {
            r=-1;
        }

        return r;
    }

	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public String getTripShortName() {
		return tripShortName;
	}

	public void setTripShortName(String tripShortName) {
		this.tripShortName = tripShortName;
	}



}
