package model.logic;

public class ParadaDistancia {
	
	Double distancia;
	VOStop parada;
	
	public ParadaDistancia(Double distancia, VOStop parada) {
		super();
		this.distancia = distancia;
		this.parada = parada;
	}
	public Double getDistancia() {
		return distancia;
	}
	public void setDistancia(Double distancia) {
		this.distancia = distancia;
	}
	public VOStop getParada() {
		return parada;
	}
	public void setParada(VOStop parada) {
		this.parada = parada;
	}
	
	
}
