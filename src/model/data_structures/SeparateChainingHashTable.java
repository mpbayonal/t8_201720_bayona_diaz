package model.data_structures;

import java.util.Iterator;

public class SeparateChainingHashTable<K extends Comparable<K>, V> 
{
	private int tamano;
	private int m;
	private DoubleLinkedListSymbolTable<K, V>[] tabla;
	
	public SeparateChainingHashTable( int tamnao ) 
	{
		
		
		this.m = tamnao;
		tabla = (DoubleLinkedListSymbolTable<K, V>[]) new DoubleLinkedListSymbolTable[m];
		for (int i = 0; i < tabla.length; i++) 
		{
			tabla [i] = new DoubleLinkedListSymbolTable<K, V>();
		}
		
		
	}
	
	public SeparateChainingHashTable(  ) 
	{
		
		
		this.m = 97;
		tabla = (DoubleLinkedListSymbolTable<K, V>[]) new DoubleLinkedListSymbolTable[m];
		for (int i = 0; i < tabla.length; i++) 
		{
			tabla [i] = new DoubleLinkedListSymbolTable<K, V>();
		}
		
		
	}
	
	public int size() 
	{
		return tamano;
	}
	
	
	
	public void cambiarTamaño(int nuevoTamaño) throws Exception 
	{
		SeparateChainingHashTable<K, V> nuevo = new SeparateChainingHashTable<K, V>(nuevoTamaño);
		for (int i = 0; i < tabla.length; i++) 
		{
			DoubleLinkedListSymbolTable<K, V> lista = tabla[i];
			if(lista.getSize()!= 0)
			{
		
			while (lista.hasNext()) 
			{
			HashNode<K, V> elemento = lista.next();
			nuevo.put(elemento.darLlave(), elemento.getValue());
			
			}
			}
			
		}
		
		this.tamano = nuevo.tamano;
		this.tabla = nuevo.tabla;
		this.m = nuevo.m;
		
		
	}
	
	public int darPrimoSiguiente(int i) {
		int r=i;

		while(true) {
			r++;
			if(esPrimo(r)) {
				break;
			}
		}

		return r;
	}
	public boolean esPrimo(int n) {
		for(int i=2;i<n;i++) {
			if(n%i==0)
				return false;
		}
		return true;
	}
	
	public void put(K llave, V valor) throws Exception
	{
		
		if(valor == null) 
		{
			delete(llave);
		}
		else if(contiene(llave)) 
		{
			int indice = hash(llave);
			
			tabla[indice].putElement(llave, valor);
		}
		else {
		if((tamano / m) >= 6) 
		{
			cambiarTamaño(darPrimoSiguiente(m));
		}
		
		int indice = hash(llave);
		
		tabla[indice].add(valor, llave);
		tamano++;
		}
		
		
	}
	
	public boolean contiene(K llave) throws Exception 
	{
		return tabla[hash(llave)].existElement(llave);
	}
	
	public V get (K llave) throws Exception 
	{
		if(llave == null)throw new Exception();
		int hashCode= hash(llave);
		return tabla[hashCode].findElement(llave);
	}
	
	
	
	public void delete(K llave) throws Exception 
	{	
		
		if(contiene(llave)) 
		{
			int posicion = hash(llave);
			tabla[posicion].delete(llave);
			tamano--;
		}
		
		
	}
	
	public int hash(K llave) throws Exception 
	{	if(llave == null)throw new Exception();
		return llave.hashCode() % m;
	}
	
	public int tamaño() 
	{
		return tamano;
	}
	
	 public Iterator<V> iterador() {
	        QueueIterable<V> cola = new QueueIterable<V>();
	        for (int i = 0; i < tabla.length; i++) 
			{
				DoubleLinkedListSymbolTable<K, V> lista = tabla[i];
				if(lista.getSize() !=0) {
				
				while (lista.hasNext()) 
				{
					HashNode<K, V> elemento = lista.next();
				cola.enqueue(elemento.getValue());
				
				}}
				
			}
	        return cola.iterator();
	    } 
	
}

