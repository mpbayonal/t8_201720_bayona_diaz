package model.data_structures;

import java.util.Iterator;

public class DFO <K extends Comparable<K> , V>
{
	private SeparateChainingHashTable<K, Boolean> marcados;
	private SeparateChainingHashTable<K, Integer> pre;
	private SeparateChainingHashTable<K, Integer> post;
	private QueueIterable<K> preOrden;
	private QueueIterable<K> posOrden;
	private int contadorPre;
	private int contadorPos;
	
	public DFO(DirectedGraph<K, V> grafo) throws Exception 
	{
		
		this.marcados = new SeparateChainingHashTable<K, Boolean>();
		this.pre =  new SeparateChainingHashTable<K , Integer> ();
		this.post = new SeparateChainingHashTable<K , Integer> ();
		this.preOrden = new QueueIterable<K>();
		this.posOrden = new QueueIterable<K>();
		contadorPos = 0;
		contadorPre = 0;
		
		Iterator<K> iterador2 = grafo.darVertices().iterator();
		
		while(iterador2.hasNext()) 
		{
			K temp = iterador2.next();
			marcados.put(temp, false);
			pre.put(temp, 0);
			post.put(temp, 0);
			
			
		}
			
		
		Iterator<K> iterador = grafo.darVertices().iterator();
		
		while(iterador.hasNext()) 
		{
			K temp = iterador.next();
			if(marcados.get(temp) == false) 
			{
				
				dfs(grafo, temp);
				
			}
			
		}
			
		
	}
	
	public Iterable<K> reversePost()
	{
		StackIterable<K> reverse = new StackIterable<K>();
		
		Iterator<K> ite = posOrden.iterator();
		while(ite.hasNext()) 
		{
			reverse.push(ite.next());
		}
		
		return reverse;
	}
	
	public void dfs (DirectedGraph<K, V> grafo , K llave) throws Exception 
	{
		marcados.put(llave, true);		
		pre.put(llave, contadorPre ++);
		preOrden.enqueue(llave);
		Iterator<EdgeNode<K, V>> temp = grafo.darAdyacentes(llave).iterator();
		
		while(temp.hasNext()) 
		{
			K llaveDest = temp.next().dest;
			if(marcados.get(llaveDest) == false) 
			{
				dfs(grafo,llaveDest );
			}
			
		}
		
		posOrden.enqueue(llave);
		post.put(llave, contadorPos ++);
		
		
	}
	
	

}
