package model.data_structures;

import java.util.Iterator;

public class DirectedGraph<K extends Comparable<K>, V> 

{


	private SeparateChainingHashTable<K, EdgeNode<K, V> > neighbors;
	private int vertexSize;
	private int edgesSize;




	public DirectedGraph() {

		vertexSize = 0;
		edgesSize = 0;
		neighbors = new SeparateChainingHashTable<K, EdgeNode<K, V> >();
	}
	
	public Iterable<EdgeNode<K, V>> darAdyacentes(K llave) throws Exception
	{
		EdgeNode<K, V> temp = neighbors.get(llave);
		QueueIterable<EdgeNode<K, V>> cola = new QueueIterable<EdgeNode<K, V>>();
		
		while(temp != null && temp.dest != null  ) 
		{
			cola.enqueue(temp);
			temp = temp.getSiguiente();
		}
		
		return cola;
		
	}
	
	public Iterable<K> darVertices() throws Exception
	{
		Iterator<EdgeNode<K, V>> temp = neighbors.iterador();
		QueueIterable<K> cola = new QueueIterable<K>();
		
		while(temp.hasNext()  ) 
		{ 	
			EdgeNode<K, V> nodo = temp.next();
			
			cola.enqueue(nodo.llaveActual);
			
		}
		
		return cola;
		
	}
	
	public V darVertex(K llave) throws Exception 
	{
		if(neighbors.get(llave)!= null)
		return neighbors.get(llave).getValor();
		else return null;
	}
	
	public boolean contieneVertex(K llave) throws Exception 
	{
		return darVertex(llave) != null;
	}



	public void addVertex(K id, V info) throws Exception 
	{
		EdgeNode<K, V> actual = neighbors.get(id);
		if(actual == null) 
		{
			EdgeNode<K, V> nuevo = new EdgeNode<K, V>(id, null, info, 0.0);
			neighbors.put(id, nuevo);
			vertexSize ++;
		}
		else 
		{
			actual.setValor(info);
		}
		
		
	}


	public boolean existeEdge(K source, K dest) throws Exception
	{
		boolean existe = false;
		if(neighbors.contiene(source)) 
		{ 	  
			EdgeNode<K, V> temp = neighbors.get(source);
			while(temp != null)
			{
				if(temp.getDest() != null && temp.getDest().compareTo(dest) == 0) 
				{
					existe = true;
				}

				temp = temp.getSiguiente();
			}
		}

		return existe;   		

	}
	
	public EdgeNode<K, V> darEdge(K source, K dest) throws Exception
	{
		EdgeNode<K, V> existe = null;
		if(neighbors.contiene(source)) 
		{ 	  
			EdgeNode<K, V> temp = neighbors.get(source);
			while(temp != null)
			{
				if(temp.getDest() != null && temp.getDest().compareTo(dest) == 0) 
				{
					existe = temp;
				}

				temp = temp.getSiguiente();
			}
		}

		return existe;  
		
	}
	
	public int vertexSize() 
	{
		return vertexSize;
	}
	public int edgeSize()
	{
		return edgesSize;
	}
	
	public DirectedGraph<K, V> grafoReverso() throws Exception
	{
		DirectedGraph<K, V> nuevo = new DirectedGraph<K,V>();
		Iterator<EdgeNode<K, V>> nuevo2 = neighbors.iterador();
		while(nuevo2.hasNext()) 
		{
			EdgeNode<K, V> temp = nuevo2.next();
			nuevo.addVertex(temp.getLlaveActual() , temp.getValor());
		}
		
		Iterator<EdgeNode<K, V>> nuevo3 = neighbors.iterador();
		while(nuevo3.hasNext()) 
		{
			EdgeNode<K, V> temp = nuevo3.next();
			Iterator<EdgeNode<K, V>>	adyacentesTemp = darAdyacentes(temp.llaveActual).iterator();
			while(adyacentesTemp.hasNext()) 
			{
				EdgeNode<K, V> temp2 = adyacentesTemp.next();
				nuevo.addEdge( temp2.dest, temp2.llaveActual, temp2.peso);
					
				
			}
			
		}
		
		
		return nuevo;
	}

	public void  addEdge( K source, K dest, double weight ) throws Exception 
	{
		EdgeNode<K, V> actual = neighbors.get(source);
		if(neighbors.contiene(source)) 
		{

			if(!existeEdge(source, dest))
			{
				if(actual.getDest() != null) 
				{
				EdgeNode<K, V> nuevo = new EdgeNode<K, V>(source, dest, null, weight);
				actual.añadirSiguiente(nuevo);
				edgesSize++;
				}
				else 
				{
					actual.setDest(dest);
					actual.setPeso(weight);
					edgesSize++;
				}
			}
		}

	}
	
	public void  addEdge( K source, K dest, double weight, V value ) throws Exception 
	{
		EdgeNode<K, V> actual = neighbors.get(source);
		if(neighbors.contiene(source)) 
		{

			if(!existeEdge(source, dest))
			{
				if(actual.getDest() != null) 
				{
				EdgeNode<K, V> nuevo = new EdgeNode<K, V>(source, dest, null, weight);
				actual.añadirSiguiente(nuevo);
				edgesSize++;
				}
				else 
				{
					actual.setDest(dest);
					actual.setPeso(weight);
					edgesSize++;
				}
			}
		}
		
		else 
		{
			EdgeNode<K, V> nuevo = new EdgeNode<K, V>(source, dest, value, weight);
			neighbors.put(source, nuevo);
			edgesSize++;
			vertexSize++;
			
		}

	}
}
