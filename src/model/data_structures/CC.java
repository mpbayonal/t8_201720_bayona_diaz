package model.data_structures;

import java.util.Iterator;

public class CC <K extends Comparable<K>, V>
{
	private SeparateChainingHashTable< K, Boolean> marcados;
	private SeparateChainingHashTable<K, Integer> id;
	private int contador;

	public CC(DirectedGraph<K, V> grafo) throws Exception {

		marcados = new SeparateChainingHashTable<K, Boolean>();
		id =  new SeparateChainingHashTable<K, Integer>();
		contador = 0;

		DFO<K , V > dfs = new DFO<K,V> (grafo.grafoReverso());
		Iterator<K> iterador2 = grafo.darVertices().iterator();

		while(iterador2.hasNext()) 
		{
			K temp = iterador2.next();
			marcados.put(temp, false);
			id.put(temp, null);			
		}

		Iterator<K> rev = dfs.reversePost().iterator();

		while(rev.hasNext()) 
		{
			K temp = rev.next();

			if(marcados.get(temp) == false) 
			{
				dfs(grafo, temp);
				contador++;
			}

		}




	}
	
	 public int count() {
	        return contador;
	    }

	    /**
	     * Are vertices {@code v} and {@code w} in the same strong component?
	     * @param  v one vertex
	     * @param  w the other vertex
	     * @return {@code true} if vertices {@code v} and {@code w} are in the same
	     *         strong component, and {@code false} otherwise
	     * @throws Exception 
	     * @throws IllegalArgumentException unless {@code 0 <= v < V}
	     * @throws IllegalArgumentException unless {@code 0 <= w < V}
	     */
	    public boolean stronglyConnected(K v, K w) throws Exception {
	       if(id.get(v) == null || id.get(w) == null) 
	       {
	    	   return false;
	    	   }
	       
	       else 
	       {
	        return id.get(v) == id.get(w);
	        }
	    }

	public void dfs (DirectedGraph<K, V> grafo , K llave) throws Exception 
	{
		marcados.put(llave, true);		
		id.put(llave, contador);

		Iterator<EdgeNode<K, V>> temp = grafo.darAdyacentes(llave).iterator();

		while(temp.hasNext()) 
		{
			K llaveDest = temp.next().dest;
			if(marcados.get(llaveDest) == false) 
			{
				dfs(grafo,llaveDest );
			}

		}




	}


}
