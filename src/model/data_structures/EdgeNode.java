package model.data_structures;

public class EdgeNode<K extends Comparable<K>,V> {
	
	K llaveActual;
	V valor;
	
	double peso;
	
	K dest;
	EdgeNode<K, V> siguiente;
	
	public EdgeNode(K dLlaveActual, K dDest, V dValor, double pPeso) {
		
		llaveActual = dLlaveActual;
		dest = dDest;
		valor = dValor;
		peso = pPeso;
		siguiente = null;
	}

	public K getLlaveActual() {
		return llaveActual;
	}

	public void setLlaveActual(K llaveActual) {
		this.llaveActual = llaveActual;
	}

	public V getValor() {
		return valor;
	}

	public void setValor(V valor) {
		this.valor = valor;
	}

	public double getPeso() {
		return peso;
	}

	public void setPeso(double peso) {
		this.peso = peso;
	}

	public K getDest() {
		return dest;
	}

	public void setDest(K dest) {
		this.dest = dest;
	}

	public EdgeNode<K, V> getSiguiente() {
		return siguiente;
	}

	public void setSiguiente(EdgeNode<K, V> siguiente) {
		this.siguiente = siguiente;
	}
	
	
	public void añadirSiguiente(EdgeNode<K, V> nuevo)
	{
		if(siguiente == null) 
		{
			siguiente = nuevo;
		}
		else 
		{
			siguiente.añadirSiguiente(nuevo);
		}
		
	}
	
	
	
	

}
