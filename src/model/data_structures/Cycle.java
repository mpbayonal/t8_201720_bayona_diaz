package model.data_structures;

import java.util.Iterator;

public class Cycle<K extends Comparable<K>, V> 
{
	private SeparateChainingHashTable<K, Boolean>  marcados;
	private SeparateChainingHashTable<K, Boolean>  enLaPila;
	private SeparateChainingHashTable<K, K>  puente;
	private StackIterable<K> pila;
	
	public Cycle(DirectedGraph<K, V> grafo) throws Exception 
	{
		marcados = new SeparateChainingHashTable<K, Boolean>();
		enLaPila = new SeparateChainingHashTable<K, Boolean>();
		puente = new SeparateChainingHashTable<K, K>();
		
		Iterator<K> ite = grafo.darVertices().iterator();
		
		while (ite.hasNext()) 
		{
			K temp = ite.next();
			marcados.put(temp, false);
			enLaPila.put(temp, false);
			puente.put(temp, null);
			
		}
		
		Iterator<K> ite2 = grafo.darVertices().iterator();
		
		while (ite2.hasNext()) 
		{
			K temp = ite2.next();
			if(marcados.get(temp) == false && pila == null ) 
			{
				dfs(grafo, temp);
			}
		}
		
		
		
	}
	
	 private void dfs(DirectedGraph<K, V> grafo, K llave) throws Exception 
	 {
		marcados.put(llave, true);
		enLaPila.put(llave, true);
		Iterator<EdgeNode<K, V>> ite = grafo.darAdyacentes(llave).iterator();
		
		while(ite.hasNext()) 
		{ EdgeNode<K, V> temp =ite.next();
		
			if(pila != null) 
			{
				return;
			}
			else if ( !marcados.get(temp.dest) ) 
			{
				puente.put(temp.dest, llave);
				dfs(grafo, temp.dest);
			}
			else if(enLaPila.get(temp.dest)) 
			{
				pila = new StackIterable<K>();
				for(K x = llave ; puente.get(x) != null && llave.compareTo(temp.dest) != 0 ; x = puente.get(x) ) 
				{
					pila.push(x);
				}
				pila.push(temp.dest);
				pila.push(llave);
			}
			
			enLaPila.put(llave, false);
			
		}
		
		 
		 
		 
	 }
	 
	 public Iterable<K> darCiclo ()
	 {
		 return pila;
	 }
	 
	 public boolean hayCiclo() 
	 {
		 return pila != null;
	 }
	
	
}
